#!/usr/bin/php
<?php
/*
 *  TODO:
 *  1. Test if uploads actually works
 *  2. Add the use of keys in ssh2
 */

if (posix_getpwuid(posix_geteuid())['name'] !== 'root') {
    echo "This script must be executed as root." . PHP_EOL;
    exit(126);
}

// Use getopt to capture date/time if set
$short_options = "d:i:nh";
$long_options = ["date:", "identity-file:", "no-action", "help"];
$options = getopt($short_options, $long_options);
$scriptName = basename(__FILE__);

if (isset($options['h']) || isset($options['help'])) {
echo <<<TEXT
Usage: {$scriptName} [OPTIONS] ...

Option \t\t GNU long option \t Meaning
-d DATE/TIME \t --date=DATE/TIME \t Specify a date/time to start search (e.g. '2024-02-12' or '2024-02-12 20:00:00')
-h \t\t --help \t\t Show this help text and exit.
-i FILE \t --identity-file=FILE\t Use an identity file (e.g. '/key/location/key-name.key')
-n \t\t --no-action \t\t Don't actually upload files; just print the steps.

TEXT;
    exit(0);
}

$failedCreatedDateTime = isset($options['d']) ? $options['d'] : (isset($options['date']) ? $options['date'] : date("Y-m-d H:i:s", strtotime("-2 hours")));
$noAction = (isset($options['n']) || isset($options['no-action'])) ? true : false;
$identityFile = null;

// Get user to chown
$currentUser = get_current_user();

// Include config
define('ROOT_PATH', "/home/{$currentUser}/");
include '/var/www/api/shared/config/config.d/production_new.php';

// Set dir variables
$origBatchFilesDir = '/var/www/api/shared/files/';
$failedTransferDir = ROOT_PATH . 'failed_transfers/';

// Create & set permissions failed transfer file folder
if(!$noAction) {
    if (!is_dir($failedTransferDir)) {
        mkdir($failedTransferDir, 0755, true);
        chown($failedTransferDir, $currentUser);
        chgrp($failedTransferDir, $currentUser);
    }
} else {
    echo "Created {$failedTransferDir} and changed the directory permissions." . PHP_EOL;
}

// Connect to pg using values from config
$dbconnStr = "host={$defaultConfig['readDbHost']} dbname={$defaultConfig['dbName']} port={$defaultConfig['dbPort']} user={$defaultConfig['dbUser']} password={$defaultConfig['dbPass']}";
$dbconn = pg_connect($dbconnStr) or die('Could not connect: ' . pg_last_error());

// Query txns and get files results
$query = <<<SQL
select f.id, f.created, f.modified, f.name, f.status, f.integration, f.type, l.partition, c."connectUsername", c."connectPassword"
from files f
inner join credentials c on f.credential = c.id
inner join entities e on c.entity = e.id
inner join logins l on e.login = l.id
where f.status = 'failed_transfer' and f.created >= '{$failedCreatedDateTime}'
SQL;

$result = pg_query($dbconn, $query) or die('Query failed: ' . pg_last_error());

// Loop results to copy files to local dir
while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) {
    $fileCreatedDate = date('Ymd', strtotime($line['created']));
    $newBatchFilesDir = "{$failedTransferDir}{$fileCreatedDate}";

    // Create the local dirs if they don't exists
    if(!$noAction) {
        if (!is_dir($newBatchFilesDir)) {
            mkdir($newBatchFilesDir, 0755, true);
            chown($newBatchFilesDir, $currentUser);
            chgrp($newBatchFilesDir, $currentUser);
        }
    } else {
        echo "Created {$newBatchFilesDir} and changed the directory permissions." . PHP_EOL;
    }

    $origBatchFile = "{$origBatchFilesDir}{$line['partition']}/Files/{$fileCreatedDate}/{$line['id']}.{$line['type']}";
    $newBatchFile = "{$newBatchFilesDir}/{$line['name']}";

    if(!$noAction) {
        if (copy($origBatchFile, $newBatchFile)) {
            chown($newBatchFile, $currentUser);
            chgrp($newBatchFile, $currentUser);
        } else {
            echo "Failed to copy {$origBatchFile} to {$newBatchFile}." . PHP_EOL;
            continue;
        }
    } else {
        echo "Copied {$origBatchFile} to {$newBatchFile} and changed the file permissions." . PHP_EOL;
    }

    // Connect to sftp server - I don't want to do this now because it's not needed
    // $remoteFile = $defaultConfig[strtolower($line['integration']) . 'BatchInboundDir'] . '/' . $line['name'];
    // $connection = ssh2_connect($defaultConfig[strtolower($line['integration']) . 'BatchFtp'], 22);
    // ssh2_auth_password($connection, $connectUsername, $connectPassword);
    // $sftp = ssh2_sftp($connection);
    // ssh2_scp_send($connection, $newBatchFile, $remoteFile);
}

// Free resultset
pg_free_result($result);

// Closing connection
pg_close($dbconn);