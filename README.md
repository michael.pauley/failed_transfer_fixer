# Failed Batch Uploads Fix Script

## Overview
This script is designed to remedy issues with batch file uploads that have failed. It performs a series of operations to identify failed batch files, copy them to a local directory, and potentially re-upload them through SFTP. 

The script requires root privileges for execution and interacts with the file system as well as a PostgreSQL database to fetch records of failed transfers.

## Requirements
- PHP CLI must be installed on the server.
- Root user access is required to execute the script.
- SSH keys and SFTP setup (if implementing the SSH/SFTP part of the script).

## Command-line Options
The script accepts several command-line options to customize its behavior:

- `-d DATE/TIME`, `--date=DATE/TIME`: Specify the start date/time for searching failed transfers. Defaults to 2 hours before the current time if not provided.
- `-i FILE`, `--identity-file=FILE`: Path to an SSH identity file (currently not used but planned for future SSH implementation).
- `-n`, `--no-action`: Simulate the script's actions without actually copying or uploading files. Useful for testing purposes.
- `-h`, `--help`: Show help information and exit.

## File and Directory Structure
The script defines necessary directories for:
- Original batch files (`/var/www/api/shared/files/`)
- Failed transfer files (user home directory followed by `/failed_transfers/`)

## Database Interaction
Connection details for the PostgreSQL database are sourced from an external configuration file. The script executes a query to retrieve failed transfer records, based on status and creation date/time threshold.

## Usage
To use this script:
```sh
sudo ./scriptname [OPTIONS]...
```
Replace `scriptname` with the actual name of the PHP script file.

### Example:
```sh
sudo ./scriptname --date="2024-02-12" --no-action
```

This will simulate fixing batch uploads failed since February 12, 2024.

## Development Status
This script is under active development. Notably, it does not yet implement SSH key-based authentication for SFTP or the actual upload mechanism. These features are mentioned in TODO comments within the script and are intended for future release.

Before running the script in a production environment, ensure that the commented-out sections relevant to your needs (especially related to SFTP operations) are implemented and thoroughly tested.

## Security Notes
Since the script operates with high-level privileges, it should be handled with care. Only authorized personnel should run this script, and necessary precautions should be taken to secure any sensitive data or access credentials involved in its operation.